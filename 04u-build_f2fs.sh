#!/bin/bash

cd $(dirname $(find ~/rpmbuild/BUILD -name .config | sort -V | tail -1))
echo "CONFIG_F2FS_FS=m" >> .config
make oldconfig
make prepare
make modules_prepare
cp -a $(find /usr/src/ -name Module.symvers | sort -V | tail -1) .
make M=fs/f2fs
strip --strip-debug fs/f2fs/f2fs.ko
