#!/bin/bash

# As instructed in https://wiki.centos.org/HowTos/I_need_the_Kernel_Source

yum -y install kernel-devel-`uname -r` asciidoc audit-libs-devel bash bc \
binutils binutils-devel bison diffutils elfutils elfutils-devel \
elfutils-libelf-devel findutils flex gawk gcc gettext gzip hmaccalc hostname \
java-devel m4 make module-init-tools ncurses-devel net-tools newt-devel \
numactl-devel openssl patch pciutils-devel perl perl-ExtUtils-Embed pesign \
python-devel python-docutils redhat-rpm-config rpm-build sh-utils tar xmlto xz \
zlib-devel

echo "Please manually install kernel src package using non-root account from http://vault.centos.org"
echo "rpm -i http://vault.centos.org/7.7.1908/updates/Source/SPackages/kernel-3.10.0-1062.9.1.el7.src.rpm"
